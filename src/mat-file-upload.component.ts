import {Component, forwardRef, HostBinding, Input, OnDestroy, Optional, Self, ViewChild} from '@angular/core';
import {MatFormFieldControl} from '@angular/material';
import {ControlValueAccessor, NgControl} from '@angular/forms';
import {coerceBooleanProperty} from '@angular/cdk/coercion';
import {FileUploadComponent as BaseFileUploadComponent} from '@dkx/ng-file-upload';
import {Subject} from 'rxjs';


@Component({
	selector: 'dkx-mat-file-upload',
	templateUrl: './mat-file-upload.component.html',
	providers: [
		{
			provide: MatFormFieldControl,
			useExisting: forwardRef(() => MatFileUploadComponent),
		},
	],
})
export class MatFileUploadComponent implements ControlValueAccessor, MatFormFieldControl<Array<Blob>>, OnDestroy
{


	private static nextId: number = 0;

	@ViewChild(BaseFileUploadComponent, {static: true})
	public fileUpload: BaseFileUploadComponent;

	@HostBinding()
	public readonly id: string = `dkx-mat-file-upload-${MatFileUploadComponent.nextId++}`;

	@HostBinding('attr.aria-describedby')
	public describedBy: string = '';

	@Input()
	public multiple: boolean = false;

	public readonly stateChanges: Subject<void> = new Subject;

	public focused: boolean = false;

	public shouldLabelFloat: boolean = true;

	private _placeholder: string = '';

	private _required: boolean = false;

	private _disabled: boolean;


	constructor(
		@Optional() @Self() public ngControl: NgControl,
	) {
		if (this.ngControl !== null) {
			this.ngControl.valueAccessor = this;
		}
	}


	@Input()
	public get value(): Array<Blob>
	{
		if (this.fileUpload) {
			return this.fileUpload.data;
		}

		return [];
	}
	public set value(value: Array<Blob>)
	{
		if (this.fileUpload) {
			this.stateChanges.next();
			this.fileUpload.setFiles(value);
		}
	}


	@Input()
	public get placeholder(): string
	{
		return this._placeholder;
	}
	public set placeholder(placeholder: string)
	{
		this._placeholder = placeholder;
		this.stateChanges.next();
	}


	@Input()
	public get required(): boolean
	{
		return this._required;
	}
	public set required(required: boolean)
	{
		this._required = coerceBooleanProperty(required);
		this.stateChanges.next();
	}


	@Input()
	public get disabled(): boolean
	{
		return this._disabled;
	}
	public set disabled(disabled: boolean)
	{
		this._disabled = coerceBooleanProperty(disabled);
		this.stateChanges.next();
	}


	public get empty(): boolean
	{
		if (this.fileUpload) {
			return this.fileUpload.data.length === 0;
		}

		return true;
	}


	public get errorState(): boolean
	{
		if (this.ngControl) {
			return this.ngControl.errors !== null;
		}

		return false;
	}


	public ngOnDestroy(): void
	{
		this.stateChanges.complete();
	}


	public setDescribedByIds(ids: string[]): void
	{
		this.describedBy = ids.join(' ');
	}


	public onContainerClick(event: MouseEvent): void {}


	public registerOnChange(fn: any): void
	{
		if (this.fileUpload) {
			this.fileUpload.registerOnChange(fn);
		}
	}


	public registerOnTouched(fn: any): void
	{
		if (this.fileUpload) {
			this.fileUpload.registerOnTouched(fn);
		}
	}


	public setDisabledState(isDisabled: boolean): void
	{
		if (this.fileUpload) {
			this.fileUpload.setDisabledState(isDisabled);
		}
	}


	public writeValue(obj: any): void
	{
		if (this.fileUpload) {
			this.fileUpload.writeValue(obj);
		}
	}


	public openUploadDialog(): void
	{
		if (this.fileUpload) {
			this.fileUpload.openDialog();
		}
	}

}
