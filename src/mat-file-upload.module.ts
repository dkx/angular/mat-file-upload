import {NgModule} from '@angular/core';
import {FileUploadModule} from '@dkx/ng-file-upload';

import {MatFileUploadComponent} from './mat-file-upload.component';


@NgModule({
	imports: [
		FileUploadModule,
	],
	declarations: [
		MatFileUploadComponent,
	],
	exports: [
		MatFileUploadComponent,
	],
})
export class MatFileUploadModule {}
