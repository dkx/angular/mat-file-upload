export * from './mat-file-upload.component';
export * from './mat-file-upload.module';

export {
	fileType,
	fileMaxSize,
} from '@dkx/ng-file-upload';
