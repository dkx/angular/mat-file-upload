# DKX/Angular/MatFileUpload

Angular material file upload component

## Installation

```bash
$ npm install --save-dev @dkx/mat-file-upload
```

or with yarn

```bash
$ yarn add @dkx/mat-file-upload
```

## Usage

**Module:**

```typescript
import {NgModule} from '@angular/core';
import {MatFileUpload} from '@dkx/mat-file-upload';

@NgModule({
    imports: [
        MatFileUpload,
    ],
})
export class AppModule {}
```

**Template:**

```html
<mat-form-field>
    <dkx-mat-file-upload #upload formControlName="files" placeholder="Image" [multiple]="true">
        <button mat-raised-button (click)="upload.openUploadDialog()" type="button">Select files</button>
        <ul>
            <li *ngFor="let file of upload.value">{{ file.name }}</li>
        </ul>
    </dkx-mat-file-upload>
</mat-form-field>
```
